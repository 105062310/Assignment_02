var loadState = {
    preload: function(){
        var loadingLabel = game.add.text(game.width/2, 150,
            'loading...', { font: '30px Arial', fill: '#ffffff' }); loadingLabel.anchor.setTo(0.5, 0.5);


        game.load.spritesheet('player', 'img/player.png', 32, 32);
        game.load.image('wall', 'img/cloud.png');
        game.load.image('wallR', 'img/cloudR.png');
        game.load.image('ceiling', 'img/cloudy.png');
        game.load.image('normal', 'img/normal.png');
        game.load.image('nail', 'img/nails.png');
        game.load.spritesheet('con_left', 'img/conveyor_left.png', 96, 16);
        game.load.spritesheet('con_right', 'img/conveyor_right.png', 96, 16);
        game.load.spritesheet('jump', 'img/trampoline.png', 96, 22);
        game.load.spritesheet('fake', 'img/fake.png', 96, 36);
        game.load.spritesheet('winds', 'img/wind.png', 141, 143);
        game.load.image('heart', 'img/heart.png');

        game.load.image('background', 'img/background.png');
        game.load.spritesheet('cliff_bg', 'img/cliff_bg.png', 385, 300);
        game.load.image('cliff_h', 'img/cliff_heaven.png');

        game.load.image('cloud_sb', 'img/cloud_sb.png');

        game.load.audio('dead', ['music/dead.mp3', 'music/dead.wav']);
        game.load.audio('music', ['music/music.mp3', 'music/music.wav']);
    },

    create: function(){
        game.physics.startSystem(Phaser.Physics.ARCADE);
        game.renderer.renderSession.roundPixels = true;
        
        game.state.start('menu');
    }
};