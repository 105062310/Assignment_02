var name_list = [];
var score_list = [];
var nameState = {
    create: function(){
        game.add.image(0, 0, 'background');

        sb();
        show();
        var startLabel = game.add.text(game.width/2-20, 470,'press the ENTER key to view more', { font: '20px Arial', fill: '#BACCD2' });
        startLabel.anchor.setTo(0.5, 0.5);
        var enterKey = game.input.keyboard.addKey(Phaser.Keyboard.ENTER);
        enterKey.onDown.add(this.start, this);
        
    }, 
    start: function(){
        game.state.start('game');
    }
};

function sb(){
    var postsRef = firebase.database().ref('sb_list');
    // List for store posts html
    
    // Counter for checking history post update complete
    var first_count = 0;
    // Counter for checking when to update new post
    var second_count = 0;
    var i = 4;
    postsRef.orderByChild('score').limitToLast(5).once('value').then(function(snapshot){
        snapshot.forEach(function(childSnapshot){
            var val = childSnapshot.val();
            console.log(name_list.length);
            name_list[i] = val.name;
            score_list[i] = val.score;
            i--;
        })
        //name_list = name_list.join('\n')
        //score_list = score_list.join('\n')
    });
    console.log(name_list[4]);
}

function show(){
    console.log(name_list);
    var input = document.getElementById('name');
    var head = '<div id = "table" class="container"><h2>Scoreboard</h2><table class="table"><thead><tr><th>Name</th><th>Score</th></tr></thead><tbody>';
    var content = [];
    for(var i = 0; i < 5; i++){
        content[content.length] = '<tr><td>' + name_list[i] + '</td><td>' + score_list[i] + '</td></tr>';
    }
    content[content.length] = '</tbody></table>Your Score: ' +  game.global.score +'</div>';
    console.log(content);
    console.log(name_list[4]);
    input.innerHTML = head + content;
}

