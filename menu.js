var menuState = {
    create: function(){
        game.add.image(0, 0, 'background');
        this.cliff = game.add.sprite(0, 230, 'cliff_bg');
        game.add.image(0, 200, 'cliff_h')
        //game.add.image(0, 200, 'cliff');
        var nameLabel = game.add.text(game.width/2, game.height/2-100, 'Stairs to Heaven',{ font: '50px Arial', fill: '#54A6C4' });
        nameLabel.anchor.setTo(0.5, 0.5);
        //var startLabel = game.add.text(game.width/2, game.height-80,'press the up arrow key to start', { font: '25px Arial', fill: '#54A6C4' });
        //startLabel.anchor.setTo(0.5, 0.5);
        //var upKey = game.input.keyboard.addKey(Phaser.Keyboard.UP);
        //upKey.onDown.add(this.start, this);

        this.cursor = game.input.keyboard.createCursorKeys();
        
        
        //this.player.body.gravity.y = 300;
        
        
        this.player = game.add.sprite(200, 250, 'player');
        this.player.checkWorldBounds = true;
        this.player.animations.add('rightwalk', [9, 10, 11, 12], 8, true);
        this.player.animations.add('leftwalk', [0, 1, 2, 3], 8, true);
        this.player.animations.add('right_fly', [26, 27, 28, 29], 8, true);
        game.physics.arcade.enable(this.player);
        game.physics.arcade.enable(this.cliff);
        this.cliff.body.immovable = true;
    },
    update: function(){
        if(this.player.x >= 350){
            this.player.animations.play('right_fly');
            this.cliff.body.checkCollision.up = false;
            this.player.body.gravity.y = 400;
        }
        game.physics.arcade.collide(this.player, this.cliff);
        this.movePlayer();
        if(this.player.y >= 500){
            this.start();
        }
    },
    start: function(){
        game.state.start('game');
    },
    movePlayer: function(){
        if(this.cursor.left.isDown){
            this.player.body.velocity.x = -200;
            this.player.animations.play('leftwalk');
        }
        else if(this.cursor.right.isDown){
            this.player.body.velocity.x = 200;
            this.player.animations.play('rightwalk');
        }
        else{
            this.player.body.velocity.x = 0;
            //this.player.animations.play('fly');
            this.player.animations.stop();
        }    
    }

    
};

