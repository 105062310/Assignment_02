var sbState = {
    create: function(){
        game.add.image(0, 0, 'background');
        this.sb = game.add.image(110, -80, 'cloud_sb');
        this.sb.scale.setTo(0.7, 0.7);
        var nameLabel = game.add.text(game.width/2-30, 80, 'Good Game!',{ font: '40px Arial', fill: '#54A6C4' });
        nameLabel.anchor.setTo(0.5, 0.5);
        //this.scoreLabel = game.add.text(38, game.height/2+70-40, 'Floor: 0',{ font: '40px Arial', fill: '#54A6C4' });
        //this.scoreLabel.anchor.setTo(0.5, 0.5); 
        this.scoreTLabel = game.add.text(380, game.height/2+150-50, 'Floor: 0',{ font: '40px Arial', fill: '#54A6C4' });
        this.scoreTLabel.anchor.setTo(0.5, 0.5); 
        var startLabel = game.add.text(game.width/2-20, 470,'press the ENTER key to view more', { font: '20px Arial', fill: '#BACCD2' });
        startLabel.anchor.setTo(0.5, 0.5);
        game.add.tween(nameLabel).to({y: 110}, 1000).easing(Phaser.Easing.Bounce.Out).start();
        game.add.tween(this.sb).to({y: -50}, 1000).easing(Phaser.Easing.Bounce.Out).start();
        //game.add.tween(this.scoreLabel).to({y: game.height/2+60}, 1000).easing(Phaser.Easing.Bounce.Out).start();
        game.add.tween(this.scoreTLabel).to({y: game.height/2+130}, 1000).easing(Phaser.Easing.Bounce.Out).start();
        
        var canvas_name = document.getElementById('name');
        var input = '<input id = "name_input" placeholder = "Your Name" onchange = upload()></input>';
        canvas_name.innerHTML = input;
        //canvas_name.style.backgroundColor = "red";
        var EnterKey = game.input.keyboard.addKey(Phaser.Keyboard.ENTER);
        EnterKey.onDown.add(upload, this);
        EnterKey.onDown.add(this.start, this);
        //this.createwind();

    },
    update: function(){
        this.scoreTLabel.text = "Floor: " + game.global.score;
        //this.wind.animations.play('fly');
    },
    start: function(){
        game.state.start('name');
    }, 
    createwind: function(){
        this.wind = game.add.sprite(game.width/2,  game.height/2,50, 'wind');
        game.physics.arcade.enable(this.wind);
        this.wind.body.gravity.y = -200;
        this.wind.animations.add('fly', [0, 1, 2], 8, true);
    }
};


function upload(){
    var postsRef = firebase.database().ref('sb_list');
    var player_name = document.getElementById('name_input').value;
    var data = {
        name: player_name,
        score: game.global.score
    }
    postsRef.push(data);
}


var game = new Phaser.Game(650, 500, Phaser.AUTO, 'canvas');
game.global = {score:0};
//game.state.add('main', mainState);
//game.state.start('main');
game.state.add('load', loadState);
game.state.add('menu', menuState);
game.state.add('game', gameState);
game.state.add('scoreboard', sbState);
game.state.add('name', nameState);
game.state.start('load');