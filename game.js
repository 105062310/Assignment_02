var cursor, lifeLabel, timeTouch = 0, timeTouchC = 0, drumSound, heart;
var gameState = {
    preload: function(){
        /*game.load.crossOrigin = 'anonymous';
        game.load.spritesheet('player', 'img/player.png', 32, 32);
        game.load.image('wall', 'img/wall.png');
        game.load.image('ceiling', 'img/ceiling.png');
        game.load.image('normal', 'img/normal.png');
        game.load.image('nail', 'img/nails.png');
        game.load.spritesheet('con_left', 'img/conveyor_left.png', 96, 16);
        game.load.spritesheet('fake', 'img/fake.png', 96, 36);

        game.load.audio('dead', ['music/dead.mp3', 'music/dead.wav']);
        game.load.audio('music', ['music/music.mp3', 'music/music.wav']);*/
    },
    create: function(){
        document.getElementById('name').innerHTML = "";
        game.add.image(0, 0, 'background');
        //game.physics.startSystem(Phaser.Physics.ARCADE);
        //game.renderer.renderSession.roundPixels = true;
        //this.cursor = game.input.keyboard.createCursorKeys();
        game.global.score = 0;
        cursor = game.input.keyboard.createCursorKeys();
        this.time = 0;
        //this.timeTouch = 0;
        //this.floor = 0;
        this.createWall();
        this.createPlayer();
        this.createStairs();
        this.createHeart();
        this.deadSound = game.add.audio('dead');
        this.musicSound = game.add.audio('music'); 
        this.musicSound.play();
        this.musicSound.loop = true;
        this.scoreLabel = game.add.text(22, 0, 'score: 0', { font: '25px Arial', fill: '#000000' });
        //lifeLabel = game.add.text(535, 0, 'life: 10', { font: '25px Arial', fill: '#ffffff' });
    },
    update: function(){
        game.physics.arcade.collide(this.player, this.walls); 
        this.movePlayer();
        game.physics.arcade.collide(this.player, this.stairs, this.change);
        game.physics.arcade.collide(this.player, this.ceiling, this.ceilingChange);
        value = game.physics.arcade.collide(this.player, this.stairs, this.change);
        if(game.time.now > this.time+500){
            this.addStairs();
            game.global.score += 1;
            this.scoreLabel.text = 'floor: ' + game.global.score; 
        }
        
        //game.physics.arcade.collide(this.player, this.stairs);
        if(!this.player.inWorld || this.player.life <= 0){this.playerDie()};
    },
    createHeart: function(){
        heart = game.add.group();
        heart.createMultiple = (5, 'heart');
        heart.scale.setTo(0.1, 0.1);
        this.heart1 = game.add.image(6200, 10, 'heart', 0, heart);
        this.heart2 = game.add.image(5950, 10, 'heart', 0, heart);
        this.heart3 = game.add.image(5700, 10, 'heart', 0, heart);
        this.heart4 = game.add.image(5450, 10, 'heart', 0, heart);
        this.heart5 = game.add.image(5200, 10, 'heart', 0, heart);
        /*console.log(this.heart.getAt(4).alive);
        this.heart5.kill();
        console.log(this.heart.getAt(4).alive);*/
    },
    createWall: function(){
        this.walls = game.add.group();
        this.walls.enableBody = true;
        this.wallR = game.add.sprite(610, 0, 'wallR', 0, this.walls);
        this.wallL = game.add.sprite(-15, 0, 'wall', 0, this.walls);
        this.wallL.scale.setTo(0.5, 1);
        this.wallR.scale.setTo(0.6, 1);
        this.ceiling = game.add.sprite(-15, -25, 'ceiling', 0, this.walls);
        this.ceiling.scale.setTo(1.1, 0.6);
        this.walls.setAll('body.immovable', true);
    },

    createPlayer: function(){
        this.player = game.add.sprite(200, 100, 'player');
        game.physics.arcade.enable(this.player);
        this.player.body.gravity.y = 300;
        this.player.animations.add('rightwalk', [9, 10, 11, 12], 8, true);
        this.player.animations.add('leftwalk', [0, 1, 2, 3], 8, true);
        this.player.animations.add('hurt', [8,17], 8, true);
        this.player.animations.add('left_hurt', [4, 5, 6, 7], 8, true);
        this.player.animations.add('right_hurt', [13, 14, 15, 16], 8, true);
        this.player.animations.add('fly', [36, 37, 39], 8, true);
        this.player.animations.add('left_fly', [18, 19, 20, 21], 8,true);
        this.player.animations.add('right_fly', [26, 27, 28, 29], 8, true);
        this.player.touchOn = false;
        this.player.life = 5;
    },

    createStairs: function(){
        this.stairs = game.add.group();
        this.stairs.enableBody = true;
    },


    addStairs: function(){
        this.time = game.time.now;
        var stairPosition = [{x:30, y:450}, {x:125, y:450}, {x:229, y:450}, {x:333, y:450}, {x:434, y:450}, {x:520, y:450}];
        var position = game.rnd.pick(stairPosition);
        if(this.time%6 == 0){
            this.stair = game.add.sprite(position.x, position.y, 'nail', 0, this.stairs);
        }
        else if(this.time%6 == 1){
            this.stair = game.add.sprite(position.x, position.y, 'con_left');
            this.stairs.add(this.stair);
            this.stair.animations.add('con_left_ani', [0, 1, 2, 3], 16, true);
            this.stair.animations.play('con_left_ani');
        }
        else if(this.time%6 == 2){
            this.stair = game.add.sprite(position.x, position.y, 'fake', 0, this.stairs);
            this.stair.animations.add('fake_ani', [0, 1, 2, 3, 4, 5, 0],  14, false);
            //this.stair.animations.play('fake_ani');
        }
        else if(this.time%6 == 3){
            this.stair = game.add.sprite(position.x, position.y, 'jump', 0, this.stairs);
            this.stair.animations.add('jump_ani', [4, 5, 4, 3, 2, 1, 0, 1, 2, 3], 120, false);
        }
        else if(this.time%6 == 4){
            this.stair = game.add.sprite(position.x, position.y, 'con_right');
            this.stairs.add(this.stair);
            this.stair.animations.add('con_right_ani', [0, 1, 2, 3], 16, true);
            this.stair.animations.play('con_right_ani');
        }
        else    
            this.stair = game.add.sprite(position.x, position.y, 'normal', 0, this.stairs);
        var i, j, flag;
        for(i = 0; i < stairPosition.length; i++){
            for(j = 0; j < this.stairs.total; j++){
                if(stairPosition[i].x == this.stairs.getAt(j).x){
                    stairPosition.splice(i, 1);
                    flag = 1;
                    break;
                }
            }
            if(flag == 1)break;   
        }
        var newPosition = game.rnd.pick(stairPosition);
        this.stair.reset(newPosition.x, newPosition.y);
        this.stair.body.velocity.y -= 100;
        this.stair.body.immovable = true;
        this.stair.checkWorldBounds = true;
        this.stair.outOfBoundsKill = true;

    },
    movePlayer: function(){
        if(cursor.left.isDown){
            this.player.body.velocity.x = -200;
            this.player.animations.play('left_fly');
        }
        else if(cursor.right.isDown){
            this.player.body.velocity.x = 200;
            this.player.animations.play('right_fly');
        }
        else{
            this.player.body.velocity.x = 0;
            this.player.animations.play('fly');
            //this.player.animations.stop();
        }    
    },
    ceilingChange: function(player, ceiling){
        if(game.time.now > timeTouchC + 500){
            player.life -= 1;
            for(var i = 0; i < 5; i++){
                if(heart.getAt(i).alive == true){
                    heart.getAt(i).kill();
                    break;
                }
            }
            //lifeLabel.text = "life: " + player.life;
            timeTouchC = game.time.now;
        }
        if(cursor.left.isDown){
            player.body.velocity.x = -200;
            player.animations.play('left_hurt');
        }
        else if(cursor.right.isDown){
            player.body.velocity.x = 200;
            player.animations.play('right_hurt');
        }
        else{
            player.body.velocity.x = 0;
            player.animations.play('hurt');
            //player.animations.stop();
        }    

    },
    
    change: function(player, stair){
        player.touchOn = true;
        if(stair.key == 'normal'){
            if(cursor.left.isDown){
                player.body.velocity.x = -200;
                player.animations.play('leftwalk');
            }
            else if(cursor.right.isDown){
                player.body.velocity.x = 200;
                player.animations.play('rightwalk');
            }
            else{
                player.body.velocity.x = 0;
                player.animations.stop();
            }   
        }
        else if(stair.key == 'nail'){
            if(game.time.now > timeTouch + 1000){
                player.life -= 1;
                for(var i = 0; i < 5; i++){
                    if(heart.getAt(i).alive == true){
                        heart.getAt(i).kill();
                        break;
                    }
                }
                timeTouch = game.time.now;
            }
            if(cursor.left.isDown){
                player.body.velocity.x = -200;
                player.animations.play('left_hurt');
            }
            else if(cursor.right.isDown){
                player.body.velocity.x = 200;
                player.animations.play('right_hurt');
            }
            else{
                player.body.velocity.x = 0;
                player.animations.play('hurt');
                //player.animations.stop();
            }    
        }
        else if(stair.key == 'con_left'){
            if(cursor.left.isDown){
                player.body.velocity.x = -250;
                player.animations.play('leftwalk');
            }
            else if(cursor.right.isDown){
                player.body.velocity.x = 50;
                player.animations.play('rightwalk');
            }
            else{
                player.body.velocity.x = -100;
                //this.player.animations.play('fly');
                player.animations.stop();
            }    
        }
        else if(stair.key == 'con_right'){
            if(cursor.left.isDown){
                player.body.velocity.x = -50;
                player.animations.play('leftwalk');
            }
            else if(cursor.right.isDown){
                player.body.velocity.x = 250;
                player.animations.play('rightwalk');
            }
            else{
                player.body.velocity.x = 100;
                //this.player.animations.play('fly');
                player.animations.stop();
            }    
        }
        else if(stair.key == 'fake'){
            stair.animations.play('fake_ani');
            stair.body.checkCollision.up = false;
            if(cursor.left.isDown){
                player.body.velocity.x = -200;
                player.animations.play('leftwalk');
            }
            else if(cursor.right.isDown){
                player.body.velocity.x = 200;
                player.animations.play('rightwalk');
            }
            else{
                player.body.velocity.x = 0;
                player.animations.play('fly');
                //this.player.animations.stop();
            }   
        }
        else if(stair.key == 'jump'){
            stair.animations.play('jump_ani');
            player.body.velocity.y = -350;
            stair.body.checkCollision.up = false;
            if(cursor.left.isDown){
                player.body.velocity.x = -200;
                player.animations.play('leftwalk');
            }
            else if(cursor.right.isDown){
                player.body.velocity.x = 200;
                player.animations.play('rightwalk');
            }
            else{
                player.body.velocity.x = 0;
                player.animations.play('fly');
                //this.player.animations.stop();
            }   
        }
    },

    playerDie: function(){
        game.state.start('scoreboard');
        game.camera.shake(0.02, 300);
        game.camera.flash(0xffffff, 300);
        this.deadSound.play();
        this.musicSound.stop();
    }


    
};
/*var game = new Phaser.Game(650, 500, Phaser.AUTO, 'canvas');
game.global = {score:0};
//game.state.add('main', mainState);
//game.state.start('main');
//game.state.add('scoreboard', sbState);
game.state.add('load', loadState);
game.state.add('menu', menuState);
game.state.add('game', gameState);

game.state.start('load');*/


